# ROOT mini cluster

## Ansible playbook

From within the CERN network, run as

```bash
ansible-playbook config_workers.yml --inventory hosts --ask-become-pass [--ask-pass]
```
